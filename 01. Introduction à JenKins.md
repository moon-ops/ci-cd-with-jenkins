Introduction à Jenkins
---

**Qu'est ce que Jenkins ?** <br>
C'est un outil de Build automation qui permet d'automatiser  <br>
les processus de déploiement d'application.<br>
Ainsi Jenkins peut permettre d'automatiser les phases de `test`, de `build`, de `deploy` <br>
Jenkins peut notifier à propos des états des processus etc  <br>
Pour l'exécution de ses différentes et pour la communication avec d'autres services <br>
Jenkins intègre plusieurs plugins.   <br>

**Installation de Jenkins** <br>

- Installation manuelle <br>

- Installation avec Docker  <br>
    - Installation de Docker <br>
    - Création du conteneur Jenkins <br>
        `docker container run -d -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home --restart always jenkins/jenkins:lts`
    
Monter Docker en tant que volume dans le conteneur <br>
```
sudo docker container run -d \
-p 8080:8080 -p 50000:50000 \
-v jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $(which docker):/usr/bin/docker \
—name jenkins \
—restart always \
jenkins/jenkins:lts
```

